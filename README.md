LuatIDE 是一个集 **单步调试** ，**智能编辑**，多方式下载等的luatOS 集成开发环境

## 快速入门
1. 获取LuatIDE 快速迭代版本([点击这里或者点击右侧发行版](https://gitee.com/openLuat/luatide/releases))
2. [安装LuatIDE](https://gitee.com/openLuat/luatide/blob/master/install.md)
3. [使用LuatIDE](https://gitee.com/openLuat/luatide/blob/master/user_guide.md)


## 寻求帮助

如果觉您需要帮助，请在留言区留下积极的反馈，或使用 **QQ** 扫描下方二维码加入LuatIDE体验群，谢谢。
![image.png](https://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210611195850299_image.png)

## 授权协议

[MIT License](LICENSE)